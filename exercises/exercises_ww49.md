---
Week: 49
tags:
- TCP
- Sockets
- HTTP(S)
- Protocols
- Socket
- Web scraping
- RFC
---

# Exercises for ww49

## Exercise 0 - PY4E chapter 13 knowledge sharing (Group) 

### Information

In your team discuss your understanding of:

2. What are RFC's (Request for comment) ?
3. What are Protocols ? (especially HTTP and HTTPS)
4. What are sockets ?
5. What is web scraping
6. When do you use a binary file instead of a text file ?
7. How does the things you have already learned, fit into chapter 12 ?

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/mDmNJBTRg9JuoHXt7](https://forms.gle/mDmNJBTRg9JuoHXt7)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## HTTP Video break

Together we will watch:

What happens when you click a link [https://youtu.be/keo0dglCj7I](https://youtu.be/keo0dglCj7I)

## Exercise 1 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 12 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak