---
Week: 45
tags:
- List
- Programming challenge
---

# Exercises for ww45

## Exercise 0 - PY4E chapter 8 knowledge (group)

### Information

In your team discuss your understanding of: 

* How do you create a list ?
* What element types can a list hold ?
* What does it mean, that lists are mutable ?
* How do you traverse a list ? 
* How can you concatenate lists ?
* What is the output of ```[7,6,5] * 2``` 
* How do you slice a list ?
* What is the return value of the pop list method ?
* How do you sum a list of numbers ?
* If you have multiple variable assigned to the same list, what impact will it have to change one of the variables ?


### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/YEWAbc82KhZBVvfT6](https://forms.gle/YEWAbc82KhZBVvfT6)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 1 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 8 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**


## Exercise 2 - Work on your own challenge

As promised, you have time scheduled in class to work on you own challenge.
Use the opportunity to ask your classmates about advice if you have problems or would like advice to solve a particular problem in your challenge.

\pagebreak