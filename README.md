![Build Status](https://gitlab.com/EAL-ITT/20a-itt1-programming/badges/master/pipeline.svg)


# 20A-ITT1-PROGRAMMING

weekly plans, resources and other relevant stuff for courses.

Links:

* [gitlab site](https://eal-itt.gitlab.io/20a-itt1-programming/)
* [Exercises gitlab group](https://gitlab.com/20a-itt1-programming-exercises)
