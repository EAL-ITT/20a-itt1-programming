# Exercise 2: Given that fruit is a string, what does fruit[:] mean?
# string slicing

fruit = 'banana'

print(fruit[3:])  # ana
print(fruit[:3])  # ban
print(fruit[:])  # banana
