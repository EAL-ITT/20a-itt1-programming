"""
Exercise 4: Add code to the above program to figure out who has the most messages in the file.
After all the data has been read and the dictionary has been created, look through the dictionary using a maximum
loop (see Chapter 5: Maximum and minimum loops) to find who has the most messages
and print how many messages the person has.

Enter a file name: mbox-short.txt
cwen@iupui.edu 5

Enter a file name: mbox.txt
zqian@umich.edu 195
"""

file_name = input('Enter a file name: ')
try:
    mails_file = open("./files/" + file_name)
    mails_histogram = dict()
    largest = None
    max_receiver = ""

    for line in mails_file:
        if line.startswith("From: "):
            address = line[line.find(" ") + 1:].rstrip()
            if address not in mails_histogram:
                mails_histogram[address] = 1
            else:
                mails_histogram[address] += 1

    mails_file.close()

    for name, i in mails_histogram.items():
        if largest is None or i > largest:
            largest = i
            max_receiver = name

    print(max_receiver, largest)

except FileNotFoundError:
    print("File not found")





