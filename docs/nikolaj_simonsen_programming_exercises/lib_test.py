# import all classes from module
# from PartyAnimal import *

# import one class
from Party import PartyAnimal

# instantiate object
an = PartyAnimal('Rudolf')

# call object method
an.party()
an.party()
an.party()

# In this variation, we access the code from within the class and explicitly pass the
# object pointer an as the first parameter (i.e., self within the method). You can
# think of an.party() as shorthand for the below line.
PartyAnimal.party(an)

# instantiate another PartyAnimal
an2 = PartyAnimal('Santa')
# call object method
an2.party()
an2.party()
an2.party()

# extend PartyAnimal class, notice PartyAnimal is used as argument and CricketFan inherits all that PartyAnimal has
class CricketFan(PartyAnimal):
    points = 0
    def six(self):
        self.points = self.points + 6
        self.party()
        print(f'{self.name} points {self.points}')

# instantiate CricketFan object called cf1
cf1 = CricketFan('Gnome')
cf1.party()
cf1.six()
# In the dir output for the j object (instance of the CricketFan class), we see that
# it has the attributes and methods of the parent class, as well as the attributes and
# methods that were added when the class was extended to create the CricketFan
# class.
print(dir(cf1))