---
Week: 45
Content:  Lists
Material: See links in weekly plan
Initials: NISI
---

# Week 45 ITT1-programming - Lists

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Chapter 8 exercises, in Python For Everybody, completed
* Work on your own challenge

### Learning goals

The student can:

* Implement Lists
* Nest lists
* Traverse lists
* Use list operations (+, *, slice)
* Use list functions
* Mutate lists 
* Use lists in functions

The student knows: 

* The list type
* When to use a list
* List methods
* List reference vs. list copy

## Deliverables

* Chapter 8 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Friday 2020-11-06 (A + B class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review 
    Solution walkthrough of the minimum system
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 8
    * Watch chapter 8 videos 
* 10:30 Hands-on time
* 11:30 Lunch break
* 12:15 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 8 video lessons:

* part 1 [https://youtu.be/ljExWqnWQvo](https://youtu.be/ljExWqnWQvo)
* part 2 [https://youtu.be/bV1FQUBIApM](https://youtu.be/bV1FQUBIApM)
* part 3 [https://youtu.be/GxADdpo6EP4](https://youtu.be/GxADdpo6EP4)