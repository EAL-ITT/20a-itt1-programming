---
Week: 46
Content:  Dictionaries
Material: See links in weekly plan
Initials: NISI
---

# Week 46 ITT1-programming - Dictionaries

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Chapter 9 exercises, in Python For Everybody, completed
* Work on your own challenge

### Learning goals

The student can:

* Implement Dictionaries
* Traverse Dictionaries
* Use Dictionary functions
* Build histograms using Dictionaries

The student knows: 

* The difference between Dictionaries and other datatypes
* When to use a Dictionary 

## Deliverables

* Chapter 9 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-11-12 (B class) and Friday 2020-11-13 (A class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 8 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 9
    * Watch chapter 9 videos 
* 10:30 Chapter 9 QUIZ
* 11:30 Lunch break
* 12:15 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 9 video lessons:

* part 1 [https://youtu.be/yDDRMb-1cxI](https://youtu.be/yDDRMb-1cxI)
* part 2 [https://youtu.be/LRSIuH94XM4](https://youtu.be/LRSIuH94XM4)
* part 3 [https://youtu.be/ZDjiFB1Ib84](https://youtu.be/ZDjiFB1Ib84)