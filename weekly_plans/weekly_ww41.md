---
Week: 41
Content:  Iteration
Material: See links in weekly plan
Initials: NISI
---

# Week 41 ITT1-programming - Iteration

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 5 in Python For Everybody
* Chapter 5 exercises, in Python For Everybody, completed

### Learning goals

The student can implement:

* Iteration
* Loops
* While
* For

The student knows:

* Loop patterns
* Debugging by bisection

## Deliverables

* Chapter 5 exercises, in Python For Everybody, documented on Gitlab
* Pair programming calculator challenge - documented on Gitlab

## Schedule

Thursday 2020-10-08 (B class) and Friday 2020-10-09 (A class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 4 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 5
    * Watch chapter 5 videos
* 10:30 Hands-on time 
* 12:15 Lunch break 
* 13:00 Hands-on time
* 16:15 End of day

## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 5 video lessons:

* part 1 [https://youtu.be/FzpurxjwmsM](https://youtu.be/FzpurxjwmsM)
* part 2 [https://youtu.be/5QDrj5ogPYc](https://youtu.be/5QDrj5ogPYc)
* part 3 [https://youtu.be/xsavQp8hd78](https://youtu.be/xsavQp8hd78)
* part 4 [https://youtu.be/yjlMMwf9Y5I](https://youtu.be/yjlMMwf9Y5I)