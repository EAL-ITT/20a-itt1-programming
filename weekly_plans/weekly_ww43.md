---
Week: 43
Content:  Strings
Material: See links in weekly plan
Initials: NISI
---

# Week 43 ITT1-programming - Strings

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 6 exercises using pair programming 
* Read chapter 6 in Python For Everybody
* Install PyCharm

### Learning goals

The student can implement:

* Parsing strings
* String methods
* Substrings

The student knows:

* The string type

## Deliverables

* Chapter 6 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-10-22 (B class) and Friday 2020-10-23 (A class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 5 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 6
    * Watch chapter 6 videos 
* 10:30 Hands-on time
* 12:15 Lunch break
* 13:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 6 video lessons:

* part 1 [https://youtu.be/dr98iM4app8](https://youtu.be/dr98iM4app8)
* part 2 [https://youtu.be/bIFpJ-qZ3Cc](https://youtu.be/bIFpJ-qZ3Cc)